<#
.SYNOPSIS
    .
.DESCRIPTION
    .
.PARAMETER Framework
    Runs tests for a specific framework, tests will be run for all frameworks specified in
    the project file
.PARAMETER TestPath
    Path to the test project or solution. It's either a path to a csproj or to a solution file or directory. If not 
    specified, it will default to "tests/"
.PARAMETER ResultsPath
    The directory where the test results are going to be placed. If not specified, it will default to "reports"
.PARAMETER ToolsPath
    The directory where the required dotnet tools will be installed. If not specified, it will default to ".tools"
.PARAMETER NonInteractive
    If specified, the script will not pause at end of execution and wait for user input
#>
param(
  [string]$Framework = $null,
  [string]$TestPath = "tests/",
  [string]$ResultsPath = "reports/",
  [string]$ToolsPath = ".tools/",
  [switch]$NonInteractive
)

# Setup
. $PSScriptRoot/common.ps1
PrintScriptTitle -Title "Unit Tests"


# Main
$ResultsPath = $ResultsPath.TrimEnd('\').TrimEnd('/')
$ToolsPath = $ToolsPath.TrimEnd('\').TrimEnd('/')

Remove-Item "$ResultsPath/*" -Recurse -Force -ErrorAction Ignore

dotnet tool install dotnet-reportgenerator-globaltool --tool-path $ToolsPath | out-null
Write-Host ""

$extraArguments = @{}

if ($Framework) {
  $extraArguments['-framework'] = $Framework
}

dotnet test $TestPath --logger "junit" --collect:"XPlat Code Coverage" --results-directory $ResultsPath @extraArguments
ThrowIfOperationFailed

Write-Host ""
& "$ToolsPath/reportgenerator" -reports:$ResultsPath/**/coverage.cobertura.xml -targetdir:$ResultsPath -reportTypes:TextSummary
& "$ToolsPath/reportgenerator" -reports:$ResultsPath/**/coverage.cobertura.xml -targetdir:$ResultsPath/html -reportTypes:Html
& "$ToolsPath/reportgenerator" -reports:$ResultsPath/**/coverage.cobertura.xml -targetdir:$ResultsPath/test -reportTypes:PngChart

Write-Host ""
Get-Content $(Join-Path -Path $ResultsPath -ChildPath "Summary.txt")


# Pause if interactive
if (!$NonInteractive) {
  PressAnyKey
}