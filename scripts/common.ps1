$ErrorActionPreference = "Stop"

function PrintScriptTitle
{
  param(
    [string]$Title
  )
  
  Write-Output "============================="
  Write-Output " $Title"
  Write-Output "============================="
}

function ThrowIfOperationFailed
{
  if (!$?) {
    throw 'Script Failed'
  }
}

function PressAnyKey
{
  Write-Host ""
  Write-Host "Press any key to continue..."
  $Host.UI.RawUI.ReadKey("NoEcho,IncludeKeyUp") > $null
}