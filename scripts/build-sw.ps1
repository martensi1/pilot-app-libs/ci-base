<#
.SYNOPSIS
    .
.DESCRIPTION
    .
.PARAMETER Configuration
    Defines the build configuration. If not specified, it will default to "Release"
.PARAMETER Framework
    Compiles for a specific framework, by default the project will be buildt for all 
    frameworks specified in the project file
.PARAMETER ProjectPath
    Path to the project or solution to build. It's either a path to a csproj or to a solution file or directory. If not 
    specified, it will default to "src"
.PARAMETER NonInteractive
    If specified, the script will not pause at end of execution and wait for user input
#>
param(
  [string]$Configuration = "Release",
  [string]$Framework = $null,
  [string]$ProjectPath = "src",
  [switch]$NonInteractive
)

# Setup
. $PSScriptRoot/common.ps1
PrintScriptTitle -Title "Build Software"


# Main
$extraArguments = @{}

if ($Framework) {
  $extraArguments['-framework'] = $Framework
}

dotnet build $ProjectPath --configuration $Configuration @extraArguments
ThrowIfOperationFailed


# Pause if interactive
if (!$NonInteractive) {
  PressAnyKey
}