<#
.SYNOPSIS
    .
.DESCRIPTION
    .
.PARAMETER Configuration
    Defines the build configuration. If not specified, it will default to "Release"
.PARAMETER PackageVersion
    The version the created package should have
.PARAMETER PackPath
    The project or solution to pack. It's either a path to a csproj or to a solution file or directory. If not 
    specified, it will default to "src"
.PARAMETER OutputDirectory
    Places the built packages in the directory specified. If not specified, it will default to "nupkgs"
.PARAMETER NonInteractive
    If specified, the script will not pause at end of execution and wait for user input
#>
param(
  [string]$Configuration = "Release",
  [Parameter(Mandatory=$true)][string]$PackageVersion,
  [string]$PackPath = "src",
  [string]$OutputDirectory = "nupkgs",
  [switch]$NonInteractive
)

# Setup
. $PSScriptRoot/common.ps1
PrintScriptTitle -Title "Package NuGet"


# Main
New-Item -Path $OutputDirectory -ItemType "directory" -Force | out-null

dotnet restore
dotnet pack --configuration $Configuration --no-build $PackPath --output $OutputDirectory /p:Version=$PackageVersion
ThrowIfOperationFailed


# Pause if interactive
if (!$NonInteractive) {
  PressAnyKey
}