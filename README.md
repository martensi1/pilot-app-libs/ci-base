# ci-base

This repository contains a common CI/CD template along with build and test scripts to be used in .NET projects


To use this template:

1. Clone this repository as an submodule to the path `/ci` in your project
2. Create an empty `.gitlab-ci.yml` file in your project and insert the following lines:
```yml
include:
  remote: 'https://gitlab.com/martensi1/pilot-app-libs/ci-base/raw/main/ci-template.yml'
```


## Background

To have a centralized repository for the CI/CD logic for all projects have multiple benefits.

- It eliminates repetetive tasks and allows for a simple way to manage all projects' build processes in one place
- Follows the DRY principle (Don't Repeat Yourself)


## Structure

- `scripts/` - .NET build and test scripts 
- `ci-template.yml` - CI/CD configuration template

## License

This repository is licensed with the [MIT](LICENSE) license

## Author

Simon Mårtensson (martensi)
